<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@Index');
Route::get('login', 'PagesController@Login');
Route::resource('sala', 'SalasController');
Route::get('/reserva/names', 'ReservasController@searchNames');
Route::resource('reserva', 'ReservasController');
Route::resource('equipo', 'EquiposController');
Route::resource('requerimiento', 'RequerimientosController');
Route::get('/calendario', function (){
    return view('reservas.calendar');
});
Route::resource('calendar', 'CalendarController');
Route::resource('buscar', 'SearchController');
Route::get('/buscar/result','SearchController@show');
Route::resource('admin','AdminController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user', 'HomeController@user');
