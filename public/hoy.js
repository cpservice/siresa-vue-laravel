(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hoy"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hoy.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hoy.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      fecha: new Date().toISOString().substr(0, 10),
      headers: [{
        text: 'Título',
        value: 'title',
        align: 'center',
        sortable: false
      }, {
        text: 'Sala',
        value: 'room',
        align: 'center',
        sortable: false
      }, {
        text: 'Descripción',
        value: 'description',
        align: 'center',
        sortable: false
      }, {
        text: 'Hora Inicio',
        value: 'init',
        align: 'center',
        sortable: false
      }, {
        text: 'Hora Fin',
        value: 'end',
        align: 'center',
        sortable: false
      } //{ text: 'Opción', value: 'option', align: 'center', sortable: false },
      ],
      reservas: [{
        title: 'Conferencia',
        room: 'Sala de cursos',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        init: '10:00am',
        end: '12:00pm',
        option: '<button>Modificar</button><br><button>Eliminar</button>'
      }, {
        title: 'Evento 12',
        room: 'Sala de conferencias',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        init: '2:00pm',
        end: '5:00pm',
        option: '<button>Modificar</button><br><button>Eliminar</button>'
      }, {
        title: 'Evento 13',
        room: 'Sala de Micros',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        init: '1:00pm',
        end: '3:00pm',
        option: '<button>Modificar</button><br><button>Eliminar</button>'
      }]
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hoy.vue?vue&type=template&id=577db337&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Hoy.vue?vue&type=template&id=577db337& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-app",
        { attrs: { id: "inspire" } },
        [
          _c(
            "v-content",
            [
              _c(
                "v-container",
                { staticClass: "fill-height", attrs: { fluid: "" } },
                [
                  _c("v-flex", { attrs: { "mb-1": "", "text-center": "" } }, [
                    _c(
                      "h3",
                      {
                        staticClass: "display-2 font-weight-bold",
                        attrs: { id: "fecha" }
                      },
                      [
                        _vm._v(
                          "\n                        " +
                            _vm._s(_vm.fecha) +
                            "\n                    "
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { attrs: { align: "center", justify: "center" } },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "8", md: "12" } },
                        [
                          _c("v-data-table", {
                            staticClass: "elevation-1",
                            attrs: {
                              headers: _vm.headers,
                              items: _vm.reservas,
                              "items-per-page": 5,
                              dark: "",
                              "show-select": ""
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Hoy.vue":
/*!*****************************************!*\
  !*** ./resources/js/components/Hoy.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Hoy.vue?vue&type=template&id=577db337& */ "./resources/js/components/Hoy.vue?vue&type=template&id=577db337&");
/* harmony import */ var _Hoy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Hoy.vue?vue&type=script&lang=js& */ "./resources/js/components/Hoy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Hoy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Hoy.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Hoy.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/components/Hoy.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Hoy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Hoy.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hoy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Hoy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Hoy.vue?vue&type=template&id=577db337&":
/*!************************************************************************!*\
  !*** ./resources/js/components/Hoy.vue?vue&type=template&id=577db337& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Hoy.vue?vue&type=template&id=577db337& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Hoy.vue?vue&type=template&id=577db337&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Hoy_vue_vue_type_template_id_577db337___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);