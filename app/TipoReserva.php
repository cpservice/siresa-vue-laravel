<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoReserva extends Model
{
    protected $table = "tipo_reservas";
    public $primaryKey = "id_tipo";
    public $timestamps = true;
}
