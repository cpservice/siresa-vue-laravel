<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requerimiento extends Model
{
    protected $table = "requerimientos";
    public $primaryKey = "id_requerimiento";
    public $timestamps = true;
}
