<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encargado extends Model
{
    protected $table = "encargados";
    public $primaryKey = "id_encargado";
    public $timestamps = true;
}
