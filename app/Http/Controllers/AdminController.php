<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sala;
use App\TipoReserva;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->load)){
            $salas = Sala::get(['nombre','id_sala','direccion','capacidad']);
            $tipos = TipoReserva::get(['type_name','id_tipo','color_type']);
            $datos = ['salas' => $salas, 'tipos'=> $tipos];

            return $datos;
        }else{
            return view('pages.admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->event)){
            $tipoReserva = new TipoReserva;
            $tipoReserva->type_name = $request->name;
            $tipoReserva->color_type = $request->color;
            $tipoReserva->save();
            return response()->json(['message' => "Tipo de evento añadido correctamente", 'cod' => 'success']);
        }else if(isset($request->room)){
            $sala = new Sala;
            $sala->nombre = $request->name;
            $sala->direccion = $request->dir;
            $sala->capacidad = $request->capacidad;
            $sala->imagen = '';
            $sala->save();
            return response()->json(['message' => "Sala añadida correctamente", 'cod' => 'success']);
        }else{
            return response()->json(['message' => "Error al añadir nueva sala",'cod' => 'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(isset($request->event)){
            $tipoReserva = TipoReserva::find($request->id);
            $tipoReserva->type_name = $request->name;
            $tipoReserva->color_type = $request->color;
            $tipoReserva->save();
            return response()->json(['message' => "Tipo de evento actualizado correctamente", 'cod' => 'success']);
        }else if(isset($request->room)){
            $sala = Sala::find($request->id);
            $sala->nombre = $request->name;
            $sala->direccion = $request->dir;
            $sala->capacidad = $request->capacidad;
            $sala->imagen = '';
            $sala->save();
            return response()->json(['message' => "Sala actualizada correctamente", 'cod' => 'success']);
        }else{
            return response()->json(['message' => "Error al actualizar",'cod' => 'error']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(isset($request->event)){
            TipoReserva::find($request->id)->forceDelete();
            return response()->json(['message' => "Tipo de evento eliminado correctamente", 'cod' => 'success']);
        }else if(isset($request->room)){
            Sala::find($request->id)->forceDelete();
            return response()->json(['message' => "Sala eliminada correctamente", 'cod' => 'success']);
        }else{
            return response()->json(['message' => "Error al eliminar",'cod' => 'error']);
        }
    }
}
