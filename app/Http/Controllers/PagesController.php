<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function Index()
	{
		return view("pages.frontpage");
	}
	public function Login()
	{
		return view("forms.login");
	}
}
