<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sala;
use App\TipoReserva;
use App\Reserva;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->load)){
            $salas = Sala::get(['nombre']);
            $tipos = TipoReserva::get(['type_name']);
            $datos = ['salas' => $salas, 'tipos'=> $tipos];

            return $datos;
        }else{
            return view('pages.search');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if($request->fecha == []){
            $reservas = Reserva::join('salas','reservas.sala','=','salas.id_sala')
            ->join('tipo_reservas','reservas.tipo','=','tipo_reservas.id_tipo')
            ->join('encargados','reservas.encargado','=','encargados.id_encargado')
            ->select('reservas.id_reserva','reservas.tesista','tipo_reservas.type_name','encargados.name','encargados.phone','encargados.email','reservas.fecha','reservas.fecha_fin','encargados.type','encargados.dir','encargados.name','reservas.hora_inicio','reservas.hora_fin','reservas.descripcion','reservas.titulo','reservas.dias','salas.nombre','salas.capacidad', 'salas.direccion')
            ->where('titulo','like','%'.$request->titulo.'%')
            ->where('type','like','%'.$request->tipo_usuario.'%')
            ->where('dir','like','%'.$request->dir_user.'%')
            ->where('nombre','like','%'.$request->sala.'%')
            ->where('name','like','%'.$request->encargado.'%')
            ->where('type_name','like','%'.$request->tipo.'%')
            ->get();
        }else{
            $reservas = Reserva::join('salas','reservas.sala','=','salas.id_sala')
            ->join('tipo_reservas','reservas.tipo','=','tipo_reservas.id_tipo')
            ->join('encargados','reservas.encargado','=','encargados.id_encargado')
            ->select('reservas.id_reserva','reservas.tesista','tipo_reservas.type_name','encargados.name','encargados.phone','encargados.email','reservas.fecha','reservas.fecha_fin','encargados.type','encargados.dir','encargados.name','reservas.hora_inicio','reservas.hora_fin','reservas.descripcion','reservas.titulo','reservas.dias','salas.nombre','salas.capacidad', 'salas.direccion')
            ->where('titulo','like','%'.$request->titulo.'%')
            ->where('type','like','%'.$request->tipo_usuario.'%')
            ->where('dir','like','%'.$request->dir_user.'%')
            ->where('nombre','like','%'.$request->sala.'%')
            ->where('name','like','%'.$request->encargado.'%')
            ->where('type_name','like','%'.$request->tipo.'%')
            ->whereBetween('fecha',$request->fecha)
            ->get();
        }
        
        return response()->json($reservas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
