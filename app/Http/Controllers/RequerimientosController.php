<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requerimiento;

class RequerimientosController extends Controller
{
    public function index()
    {
        $salas = Sala::all();
        return view('salas.index')->with('salas',$salas);
    }
    
    public function create()
    {
        return view('salas.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'Nombre' => 'required',
            'Direccion' => 'required',
            'Capacidad' => 'required',
            'Imagen' => 'image|nullable|max:1999'
        ]);

        if ($request->hasFile('Imagen'))
        {
            $filenameWithExt = $request->file('Imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('Imagen')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('Imagen')->storeAs('public/salas_imagenes',$filenameToStore);
        }
        else
        {
            $filenameToStore = 'logo.png';
        }

        $sala = new Sala;
        $sala->nombre = $request->input('Nombre');
        $sala->direccion = $request->input('Direccion');
        $sala->capacidad = $request->input('Capacidad');
        $sala->imagen = $filenameToStore;
        $sala->save();

        return redirect('/sala')->with('success', "Sala Registrada");
    }

    public function show($id)
    {
        $sala = Sala::find($id);
        return view('salas.show')->with('sala',$sala);
    }

    public function edit($id)
    {
        $sala = Sala::find($id);
        return view('salas.edit')->with('sala',$sala);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Nombre' => 'required',
            'Direccion' => 'required',
            'Capacidad' => 'required',
            'Imagen' => 'image|nullable|max:1999'
        ]);

        if ($request->hasFile('Imagen'))
        {
            $filenameWithExt = $request->file('Imagen')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('Imagen')->getClientOriginalExtension();
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('Imagen')->storeAs('public/salas_imagenes',$filenameToStore);
        }

        $sala = Sala::find($id);
        $sala->nombre = $request->input('Nombre');
        $sala->direccion = $request->input('Direccion');
        $sala->capacidad = $request->input('Capacidad');
        if ($request->hasFile('Imagen'))
        {
            $sala->imagen = $filenameToStore;
        }
        $sala->save();

        return redirect('/sala')->with('success', "Sala Actualizada");
    }

    public function destroy($id)
    {
        $sala = Sala::find($id);
        $sala->delete();
        return redirect('/sala')->with('success', 'Sala Eliminada');
    }
}
