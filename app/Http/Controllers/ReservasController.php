<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserva;
use App\Sala;
use App\TipoReserva;
use App\Encargado;

class ReservasController extends Controller
{
    public function index(Request $request)
    {

        $reservas = Reserva::Join('salas','reservas.sala','=','salas.id_sala')
            ->join('tipo_reservas','reservas.tipo','=','tipo_reservas.id_tipo')
            ->join('encargados','reservas.encargado','=','encargados.id_encargado')
            ->select('reservas.*','salas.*','tipo_reservas.*','encargados.*')
            ->where('fecha_fin','>=',$request->fecha)
            ->where('fecha','<=',$request->fecha)
            ->orderBy('hora_inicio')
            ->get();

        return response()->json($reservas);
    }

    public function create(Request $request)
    {
        if(isset($request->load)){
            $salas = Sala::get(['id_sala','nombre']);
            $tipos = TipoReserva::get(['id_tipo','type_name']);
            $datos = ['salas' => $salas, 'tipos'=> $tipos];
            
            return response()->json($datos);
        }else{
            return view('reservas.create');//->with('salas',$salas);
        }
    }

    public function searchNames()
    {
        $names = Encargado::get('name');
        return response()->json($names);
    }

    public function store(Request $request)
    {
        $sala = Sala::where('nombre','=',$request->sala_event)->get(['id_sala']);
        $type = TipoReserva::where('type_name','=',$request->event_type)->get(['id_tipo']);
        // $veriReserva = Reserva::where('fecha','=',$request->date_init)
        //     ->where('hora_inicio','<=',$request->time_init)
        //     ->orWhere('hora_inicio','<',$request->time_end)
        //     ->where('hora_fin','>',$request->time_init)
        //     ->orWhere('hora_fin','>=',$request->time_end)
        //     ->get(['sala']);


        // if(isset($veriReserva[0]->sala)){
        //     if($veriReserva[0]->sala == $sala[0]->id_sala){
        //         return "Sala";
        //     }
        // }
        
        $reserva = new Reserva;
        $reserva->titulo = $request->title_event;
        $reserva->tipo = $type[0]->id_tipo;
        $reserva->sala = $sala[0]->id_sala;
        $reserva->fecha = $request->date_init;
        $reserva->fecha_fin = $request->date_end;
        $reserva->hora_inicio = $request->time_init;
        $reserva->hora_fin = $request->time_end;
        $reserva->descripcion = $request->desc_event;

        if($request->isTesis) {
            $reserva->tesista = $request->tesista;
        }
        
        if($request->day == NULL){
            $reserva->dias = NULL;
        }else{
            $reserva->dias = implode(",",$request->day);
        }

        $solicitante = Encargado::where('name','=',$request->name)->get(['id_encargado']);

        

        if(!isset($solicitante[0]->id_encargado)){
            $solicitante = new Encargado;
            $solicitante->name = $request->name;
            $solicitante->phone = $request->phone;
            $solicitante->email = $request->correo;
            $solicitante->type = $request->tipo_usuario;
            $solicitante->dir = $request->ubicacion;

            $solicitante->save();
            $reserva->encargado = $solicitante->id_encargado;
        }else{
            $reserva->encargado = $solicitante[0]->id_encargado;
        }      
        
        $reserva->creado_por = auth()->user()->id;
        $reserva->save();

        return "OK";
    }

    public function show($id)
    {
        $reserva = Reserva::find($id);
        return view('reservas.show')->with('reserva',$reserva);
    }

    public function edit(Request $request)
    {

        if(isset($request->load)){
            $reserva = Reserva::where('id_reserva','=',$request->id)
            ->join('salas','reservas.sala','=','salas.id_sala')
            ->join('tipo_reservas','reservas.tipo','=','tipo_reservas.id_tipo')
            ->join('encargados','reservas.encargado','=','encargados.id_encargado')
            ->select('reservas.*','salas.*','tipo_reservas.*','encargados.*')
            ->get();

            return response()->json($reserva);
        }
        

        return view('reservas.edit');
    }

    public function update(Request $request)
    {
        $reserva = Reserva::find($request->id);
        $sala = Sala::where('nombre','=',$request->sala_event)->get(['id_sala']);
        $type = TipoReserva::where('type_name','=',$request->event_type)->get(['id_tipo']);
        // $veriReserva = Reserva::where('fecha','=',$request->date_init)
        //     ->where('hora_inicio','<=',$request->time_init)
        //     ->orWhere('hora_inicio','<',$request->time_end)
        //     ->where('hora_fin','>',$request->time_init)
        //     ->orWhere('hora_fin','>=',$request->time_end)
        //     ->where('id_reserva','><',$request->id)
        //     ->get(['sala']);


        // if(isset($veriReserva[0]->sala)){
        //     if($veriReserva[0]->sala == $sala[0]->id_sala){
        //         return "Sala";
        //     }
        // }

        $reserva->titulo = $request->title_event;
        $reserva->tipo = $type[0]->id_tipo;
        $reserva->sala = $sala[0]->id_sala;
        $reserva->fecha = $request->date_init;
        $reserva->fecha_fin = $request->date_end;
        $reserva->hora_inicio = $request->time_init;
        $reserva->hora_fin = $request->time_end;
        $reserva->descripcion = $request->desc_event;

        if($request->isTesis) {
            $reserva->tesista = $request->tesista;
        }else{
            $reserva->tesista = NULL;
        }
        
        if($request->day == NULL){
            $reserva->dias = NULL;
        }else{
            $reserva->dias = implode(",",$request->day);
        }

        $solicitante = Encargado::where('name','=',$request->name)->get(['id_encargado']);


        if(!isset($solicitante[0]->id_encargado)){

            $solicitante = new Encargado;
            $solicitante->name = $request->name;
            $solicitante->phone = $request->phone;
            $solicitante->email = $request->correo;
            $solicitante->type = $request->tipo_usuario;
            $solicitante->dir = $request->ubicacion;

            $solicitante->save();
            $reserva->encargado = $solicitante->id_encargado;
        }else{
            $reserva->encargado = $solicitante[0]->id_encargado;
            $update_encargado = Encargado::find($solicitante[0]->id_encargado);
            $update_encargado->name = $request->name;
            $update_encargado->phone = $request->phone;
            $update_encargado->email = $request->correo;
            $update_encargado->type = $request->tipo_usuario;
            $update_encargado->dir = $request->ubicacion;

            $update_encargado->updated_at = auth()->user()->id;
            $update_encargado->save();
        }

        $reserva->actualizado_por = auth()->user()->id;
        $reserva->save();

        return "OK";
    }

    public function destroy(Request $request)
    {
        $Reserva = Reserva::find($request->id);
        $id_encargado = $Reserva->encargado;
        
        $Reserva->delete();

        $reservas_asociadas = Reserva::where('encargado','=',$id_encargado)->get(['encargado']);

        if(!isset($reservas_asociadas[0]->encargado)){
            $encargado = Encargado::find($id_encargado);

            $encargado->delete();
        }

        return "OK";
    }
}
