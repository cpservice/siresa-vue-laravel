<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;

class EquiposController extends Controller
{
    public function index()
    {
        $equipos = Equipo::all();
        return view('equipos.index')->with('equipos',$equipos);
    }

    public function create()
    {
        return view('equipos.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'Nombre' => 'required',
            'Descripcion' => 'required',
            'Cantidad' => 'required'
        ]);

        $Equipo = new Equipo;
        $Equipo->nombre = $request->input('Nombre');
        $Equipo->descripcion = $request->input('Descripcion');
        $Equipo->cantidad = $request->input('Cantidad');
        $Equipo->save();

        return redirect('/equipo')->with('success', "Equipo Registrado");
    }

    public function show($id)
    {
        $equipo = Equipo::find($id);
        return view('equipos.show')->with('equipo',$equipo);
    }

    public function edit($id)
    {
        $equipo = Equipo::find($id);
        return view('equipos.edit')->with('equipo',$equipo);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'Nombre' => 'required',
            'Descripcion' => 'required',
            'Cantidad' => 'required',
        ]);

        $Equipo = Equipo::find($id);
        $Equipo->nombre = $request->input('Nombre');
        $Equipo->descripcion = $request->input('Descripcion');
        $Equipo->cantidad = $request->input('Cantidad');
        $Equipo->save();

        return redirect('/equipo')->with('success', "Equipo Actualizado");
    }

    public function destroy($id)
    {
        $Equipo = Equipo::find($id);
        $Equipo->delete();
        return redirect('/equipo')->with('success', 'Equipo Eliminado');
    }
}
