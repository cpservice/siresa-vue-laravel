<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $table = "equipos";
    public $primaryKey = "id_equipo";
    public $timestamps = true;
}
