<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = "reservas";
    public $primaryKey = "id_reserva";
    public $timestamps = true;

    public function ubicacion()
    {
    	return $this->belongsTo('App\Sala','sala','id_sala');
    }

    public function creador()
    {
    	return $this->belongsTo('App\User','creado_por','id');
    }

    public function type(){
        return $this->belongsTo('App\TipoReserva','tipo','id_tipo');
    }

    public function encargado(){
        return $this->belongsTo('App\Encargado','encargado','id_encargado');
    }
}
