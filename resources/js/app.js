import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css';
//import VueRouter from 'vue-router'

require('./bootstrap');

window.Vue = require('vue');
Vue.use(Vuetify)
Vue.use(require('vue-moment'));

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('layout-primary', require('./components/MenuComponent.vue').default);

Vue.component('hoy', require('./components/Hoy.vue').default);

Vue.component('calendar', require('./components/Calendar.vue').default);

Vue.component('login', require('./components/Login.vue').default);

Vue.component('register', require('./components/Register.vue').default);

Vue.component('search', require('./components/Search.vue').default);

Vue.component('new', require('./components/Nuevo.vue').default);

Vue.component('admin', require('./components/Admin.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.use(VueRouter)

/*const router = new VueRouter({
    mode: 'history',
    routes: [
        // {
        //     path: '/',
        //     name: 'hoy',
        //     //component: Home,
        //     component: () => import(/* webpackChunkName: "hoy" */ //'./components/Hoy.vue')
        //   },
        //   {
        //     path: '/login',
        //     name: 'login',
        //     component: () => import(/* webpackChunkName: "login" */ './components/Login.vue')
        //   },
        //   {
        //     path: '/calendar',
        //     name: 'calendario',
        //     component: () => import(/* webpackChunkName: "calendario" */ './components/Calendar.vue')
        //   },
        //   {
        //     path: '/admin',
        //     name: 'administración',
        //     component: () => import(/* webpackChunkName: "eventos" */ './components/Add-Event.vue')
        //   },
        //   {
        //     path: '/search',
        //     name: 'buscar',
        //     component: () => import(/* webpackChunkName: "buscar" */ './components/Search.vue')
        //   },
        //   {
        //     path: '/new',
        //     name: 'Nuevo',
        //     component: () => import(/* webpackChunkName: "nuevo" */ './components/Nuevo.vue')
        //   }
   /* ],
});*/



const app = new Vue({
    vuetify: new Vuetify(),
    el: '#app',
    //router,
});
