@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<div class="row">
		<hr class='col-11'>
		<h3 class='col-10'>Nuevo Equipo</h3><a class='btn btn-warning col-2' href='/equipo'>Regresar</a>
		<hr class='col-11'>
		<div class='col-12'>
		{!! Form::open(['action' => 'EquiposController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
			<div class='form-group'>
				{{Form::label('Nombre','Nombre')}}
				{{Form::text('Nombre', '', ['class' => 'form-control', 'placeholder' => 'Nombre del equipo'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Descripcion','Descripcion')}}
				{{Form::text('Descripcion', '', ['class' => 'form-control', 'placeholder' => 'Descripcion del equipo'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Cantidad','Cantidad')}}
				{{Form::text('Cantidad', '', ['class' => 'form-control', 'placeholder' => 'Cantidad en inventario'])}}
			</div>
			{{Form::submit('Registrar', ['class' => 'btn btn-primary'])}}
		{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection