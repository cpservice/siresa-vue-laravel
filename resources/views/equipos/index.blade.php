@extends('layouts.app')

@section('content')
<div class="rounded-lg  col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<hr>
	<div class="row">
		<h3 class="col-md-9">Equipos</h3>
		<a class="col-md-3 btn btn-primary" href="/equipo/create">Registrar Equipo</a>
	</div>
	<hr>
	@if (count($equipos) > 0)
		<table class="table text-center table-responsive-md table-hover">
		<tbody>
			<tr>
				<th scope="col">Nombre</th>
				<th scope="col">Descripcion</th>
				<th scope="col">Cantidad</th>
				<th scope="col">Acciones</th>
			</tr>
			@foreach ($equipos as $equipo)
				<tr>
					<th scope="row"><a href='equipo/{{$equipo->id_equipo}}'>{{$equipo->nombre}}</a></th>
					<th>{{$equipo->descripcion}}</th>
					<th>{{$equipo->cantidad}}</th>
					<th><a class='btn btn-warning' href='equipo/{{$equipo->id_equipo}}/edit'>Editar</a></th>
				</tr>
			@endforeach
		</tbody>
	</table>
	@else
		<h4> No hay equipos registrados </h4>
	@endif
</div>
@endsection