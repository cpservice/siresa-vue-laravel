@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<hr>
	<div class="row">
		<hr>
		<h3 class='col-10'>{{$equipo->nombre}}</h3><a class='btn btn-warning col-2' href='/equipo'>Regresar</a>
		<p class='text-justify col-12'><b>Descripcion:</b> {{$equipo->descripcion}}</p><br>
		<p class='text-justify col-12'><b>Cantidad:</b> {{$equipo->cantidad}}</p><br>
		{!!Form::open(['action' => ['EquiposController@destroy', $equipo->id_equipo], 'method' => 'POST'])!!}
		{{Form::hidden('_method','DELETE')}}
		{{Form::submit('Eliminar Equipo', ['class' => 'btn col-12 btn-danger'])}}
		{!!Form::close()!!}
	</div>
</div>
@endsection