@extends('layouts.app')

@section('content')
<div class="container pt-12">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Escritorio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Has iniciado correctamente!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
