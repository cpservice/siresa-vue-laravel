@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto p-4" style="background-color:white; color:black;">
<hr>
	<div class="row">
		<h3 class='col-10'>Reserva de Sala</h3>
		<a class='btn btn-warning col-2' href='/reserva'>Regresar</a>
		<hr class='col-11'>
		<ul class='col-12 px-5'>
			<li><h3 class='d-inline'>{{$reserva->ubicacion->nombre}}</h3></li>
			<li>Fecha: {{$reserva->fecha}}</li>
			<li>Hora de Inicio: {{$reserva->hora_inicio}}</li>
			<li>Hora Limite: {{$reserva->hora_fin}}</li>
			<li>Encargado: {{$reserva->encargado}}</li>
			<li>Contacto: {{$reserva->contacto}}</li>
			<li>Descripcion: {{$reserva->descripcion}}</li>
			<li>Creado Por: {{$reserva->creado_por}}</li>
		</ul>
		{!!Form::open(['action' => ['ReservasController@destroy', $reserva->id_reserva], 'method' => 'POST'])!!}
		{{Form::hidden('_method','DELETE')}}
		{{Form::submit('Eliminar Reservación', ['class' => 'btn col-12 btn-danger'])}}
		{!!Form::close()!!}
		<a class='btn btn-warning col-auto' href='reserva/{{$reserva->id_reserva}}/edit'> Editar </a>
	</div>
</div>
@endsection