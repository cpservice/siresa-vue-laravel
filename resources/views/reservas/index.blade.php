@extends('layouts.app')

@section('content')
<div class="rounded-lg  col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<hr>
	<div class="row">
		<h3 class="col-md-9">Reservas</h3>
		<a class="col-md-3 btn btn-primary" href="/reserva/create">Reservar Sala</a>
	</div>
	<hr>
	@if (count($reservas) > 0)
		<ul class='col-12'>
		@foreach ($reservas as $reserva)
			<li>
				<h3 class='d-inline'>
					<a href='reserva/{{$reserva->id_reserva}}'>
						{{$reserva->ubicacion->nombre}}
					</a>
				</h3> - {{$reserva->fecha}} - 
			</li>
		@endforeach
		</ul>
	@else
		<h4> No hay reservas registradas </h4>
	@endif
</div>
@endsection