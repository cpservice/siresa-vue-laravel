

<html lang="es" style='scroll-behavior: smooth;'>
	<head>
		<meta charset='UTF-8'>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'>
		<title id='HTMLTitulo'>{{config('app.name', "Error")}}</title>
		<meta name='description' content='Descripcion'>
		<meta name='keywords' content='Palabras Clave'>
		<link rel='shortcut icon' href='{{asset("storage/favicon.png")}}' type='image/png'>
		<link rel='stylesheet' href='{{asset("css/s3.css")}}'>
		<link rel='stylesheet' href='{{asset("css/bootstrap.css")}}'>
		<script src='../global/js/aFetch.js'></script>
		<script src='../global/js/jquery.js'></script>
		<script src='../global/js/bootstrap.js'></script>
		<script src='../global/js/parallax.js'></script>
	</head>
	<body style='background-color:white;' class='text-white'>
	<progress style='height:1vh; background-color:red;' value='2' max='4' id='fetchProgress' class='fixed-top w-100'></progress>
	<div id='pageTop' style='height:10vh;' class='d-block col-12'></div>
	<nav style='height:10vh; background-color:royalblue; box-shadow: 0 0 10px black' style='z-index:50;' class='navbar text-light navbar-expand-lg container-fluid main fixed-top'>
		<a class='m-1 my-auto p-auto navbar-brand align-middle' style='color:white' href='/'>
			<img src='{{asset("storage/logo.png")}}' style='height:5vh;'	class='d-inline align-top' alt='Logo'>
			<span class='m-3 align-middle'>{{config('app.name', "Error")}}</span>
		</a>
		<button class='navbar-toggler mx-3' type='button' data-toggle='collapse' data-target='#navBarM' aria-controls='navbarNav' aria-expanded='false' aria-label='Toggle navigation'>
			<span class='navbar-toggler-icon'></span>
		</button>
		<div class='collapse navbar-collapse rounded justify-content-end' id='navBarM'>
			<ul class='navbar-nav'>
				<li class='nav-item'>
					<a style='color:white' href='calendario' class='btn col-sm-12 nav-link'>Calendario</a>
				</li>
				<li class='nav-item'>
					<a style='color:white' href='sala' class='btn col-sm-12 nav-link'>Salas</a>
				</li>
				<li class='nav-item'>
					<a style='color:white' href='equipo' class='btn col-sm-12 nav-link'>Equipos</a>
				</li>
				<li class='nav-item'>
					<a style='color:white' href='requerimiento' class='btn col-sm-12 nav-link'>Requerimientos</a>
				</li>
				<li class='nav-item'>
					<a style='color:white' href='login' class='btn col-sm-12 nav-link'>Iniciar Sesión</a>
				</li>
			</ul>
		</div>
	</nav>
	<div class='container-fluid'>
		<div class='row'>
			<div class='rounded-lg col-sm-0 col-md-0 col-xl-1' id='leftSpace' style='background-color:#0000; color:#0000'></div>
			<div class='rounded-lg col-sm-12 col-md-12 col-xl-10' id='contenido-marco' style='background-color:white; color:black'>
				<br>
				@include('inc.messages')
				@yield('content')
			</div>
			<div class='rounded-lg col-sm-0 col-md-0 col-xl-1' id='rightSpace' style='background-color:#0000; color:#0000'></div>
		</div>
	</div>
	<div style='background-color:royalblue; color:white; box-shadow: 0 0 10px black' class='fixed-bottom h-auto w-100 text-right px-3 footer'>Centro de Computacion ©<?php echo date('Y')?></div>
	<script></script>
</body>
</html>