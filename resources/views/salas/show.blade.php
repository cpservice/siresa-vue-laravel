@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<hr>
	<div class="row">
		<hr>
		<h3 class='col-10'>{{$sala->nombre}}</h3><a class='btn btn-warning col-2' href='/sala'>Regresar</a>
		<p class='text-justify col-12'><b>Direccion:</b> {{$sala->direccion}}</p><br>
		<p class='text-justify col-12'><b>Capacidad:</b> {{$sala->capacidad}}</p><br>
		<img src='/storage/salas_imagenes/{{$sala->imagen}}' class='col-10'>
		{!!Form::open(['action' => ['SalasController@destroy', $sala->id_sala], 'method' => 'POST'])!!}
		{{Form::hidden('_method','DELETE')}}
		{{Form::submit('Eliminar Ubicacion', ['class' => 'btn col-12 btn-danger'])}}
		{!!Form::close()!!}
	</div>
</div>
@endsection