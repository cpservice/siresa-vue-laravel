@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<div class="row">
		<hr class='col-11'>
		<h3 class='col-10'>Editar Ubicacion</h3><a class='btn btn-warning col-2' href='/sala'>Regresar</a>
		<hr class='col-11'>
		<div class='col-12'>
		{!! Form::open(['action' => ['SalasController@update', $sala->id_sala], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
			<div class='form-group'>
				{{Form::label('Nombre','Nombre')}}
				{{Form::text('Nombre', $sala->nombre, ['class' => 'form-control', 'placeholder' => 'Nombre de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Direccion','Direccion')}}
				{{Form::text('Direccion', $sala->direccion, ['class' => 'form-control', 'placeholder' => 'Direccion de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Capacidad','Capacidad')}}
				{{Form::text('Capacidad', $sala->capacidad, ['class' => 'form-control', 'placeholder' => 'Capacidad de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::file('Imagen')}}
			</div>
			{{Form::hidden('_method','PUT')}}
			{{Form::submit('Actualizar', ['class' => 'btn btn-primary'])}}
		{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection