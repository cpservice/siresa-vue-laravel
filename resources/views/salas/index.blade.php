@extends('layouts.app')

@section('content')
<div class="rounded-lg  col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<hr>
	<div class="row">
		<h3 class="col-md-9">Ubicaciones</h3>
		<a class="col-md-3 btn btn-primary" href="/sala/create">Registrar Ubicación</a>
	</div>
	<hr>
	@if (count($salas) > 0)
		<table class="table text-center table-responsive-md table-hover">
		<tbody>
			<tr>
				<th scope="col">Nombre</th>
				<th scope="col">Dirección</th>
				<th scope="col">Capacidad</th>
				<th scope="col">Acciones</th>
			</tr>
			@foreach ($salas as $sala)
				<tr>
					<th scope="row"><a href='sala/{{$sala->id_sala}}'>{{$sala->nombre}}</a></th>
					<th>{{$sala->direccion}}</th>
					<th>{{$sala->capacidad}}</th>
					<th><a class='btn btn-warning' href='sala/{{$sala->id_sala}}/edit'>Editar</a></th>
				</tr>
			@endforeach
		</tbody>
	</table>
	@else
		<h4> No hay ubicaciones registradas </h4>
	@endif
</div>
@endsection