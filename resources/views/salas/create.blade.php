@extends('layouts.app')

@section('content')
<div class="rounded-lg col-auto" style="background-color:white; color:black;" id="contenido-marco">
	<div class="row">
		<hr class='col-11'>
		<h3 class='col-10'>Nueva Ubicacion</h3><a class='btn btn-warning col-2' href='/sala'>Regresar</a>
		<hr class='col-11'>
		<div class='col-12'>
		{!! Form::open(['action' => 'SalasController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
			<div class='form-group'>
				{{Form::label('Nombre','Nombre')}}
				{{Form::text('Nombre', '', ['class' => 'form-control', 'placeholder' => 'Nombre de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Direccion','Direccion')}}
				{{Form::text('Direccion', '', ['class' => 'form-control', 'placeholder' => 'Direccion de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::label('Capacidad','Capacidad')}}
				{{Form::text('Capacidad', '', ['class' => 'form-control', 'placeholder' => 'Capacidad de la ubicacion'])}}
			</div>
			<div class='form-group'>
				{{Form::file('Imagen')}}
			</div>
			{{Form::submit('Registrar', ['class' => 'btn btn-primary'])}}
		{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection