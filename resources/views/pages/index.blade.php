<!DOCTYPE html>
<html lang="es" style='scroll-behavior: smooth;'>
	<head>
		<meta charset='UTF-8'>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'>
		<title id='HTMLTitulo'>{{config('app.name', "SIRESA")}}</title>
		<meta name='description' content='Descripcion'>
		<meta name='keywords' content='Palabras Clave'>
		<link rel='shortcut icon' href='img/favicon.png' type='image/png'>
		<link rel='stylesheet' href='../global/css/s3.css'>
		<link rel='stylesheet' href='../global/css/bootstrap.css'>
		<script src='../global/js/aFetch.js'></script>
		<script src='../global/js/jquery.js'></script>
		<script src='../global/js/bootstrap.js'></script>
		<script src='../global/js/parallax.js'></script>
	</head>
	<body style='background-color:white;' class='text-white'>
	<progress style='height:1vh; background-color:red;' value='2' max='4' id='fetchProgress' class='fixed-top w-100'></progress>
	<div id='pageTop' style='height:10vh;' class='d-block col-12'></div>
	<nav style='height:10vh; background-color:royalblue; box-shadow: 0 0 10px black' style='z-index:50;' class='navbar text-light navbar-expand-lg container-fluid main fixed-top'>
		<a class='m-1 my-auto p-auto navbar-brand align-middle' style='color:white' href='#pageTop'>
			<img src='img/logo.png'	style='height:5vh;'	class='d-inline align-top' alt='Logo'>
			<span class='m-3 align-middle'>Centro de Computacion</span>
		</a>
		<button class='navbar-toggler mx-3' type='button' data-toggle='collapse' data-target='#navBarM' aria-controls='navbarNav' aria-expanded='false' aria-label='Toggle navigation'>
			<span class='navbar-toggler-icon'></span>
		</button>
		<div class='collapse navbar-collapse rounded justify-content-end' id='navBarM'>
			<ul class='navbar-nav'>
				<li class='nav-item'>
					<button style='color:white' id='navItem0' class='btn col-sm-12 nav-link' data-toggle='modal' data-target='#modalM' onclick='aFetch("login","contenido-modal","GET")'>Iniciar Sesión</button>
				</li>
			</ul>
		</div>
	</nav>
	<div class='modal' id='modalM' tabindex='-1' role='dialog'>
		<div class='modal-dialog modal-lg' role='document'>
			<div class='modal-content' style='background-color:white; color:black;' id='contenido-modal'>
			</div>
		</div>
	</div>
	<header class='container-fluid'>
		<div class='row'>
			<div class='rounded-lg  col-12 ' style='background-color:white; color:black;' id='contenido-marco'>
			</div>
		</div>
	</header>
	<div style='background-color:royalblue; color:white; box-shadow: 0 0 10px black' class='fixed-bottom h-auto w-100 text-right px-3 footer'>
		Centro de Computacion ©2019
	</div>
	<script>
		aFetch("frontpage","contenido-marco","GET");
		/*aFetch("pag/sidenav_menu.php","contenido-sidenav",null,false,() => {
			let sidenav = document.getElementById("contenido-sidenav");
			if (sidenav.innerHTML == "")
			{
				sidenav.remove();
				document.getElementById("contenido-marco").classList.remove("col-md-9");
			}
		}); 
		$('#modalM').on('hidden.bs.modal', function ()
		{
			$("#contenido-modal").html("");
		})*/
	</script>
	</body>
</html>