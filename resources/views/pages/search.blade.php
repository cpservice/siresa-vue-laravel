@extends('layouts.app')

@guest

	@section('out')
		<login>
			{{ csrf_field() }}
		</login>
	@endsection

@else
	@section('content')

		<search></search>

	@endsection
@endguest