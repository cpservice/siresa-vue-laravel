<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsuarioActualizaReserva extends Migration
{
    public function up()
    {
        Schema::table('reservas', function($table)
        {
            $table->dropColumn('usuario_responsable');
            $table->integer('creado_por');
            $table->integer('actualizado_por')->nullable();
        });
    }

    public function down()
    {
        Schema::table('reservas', function($table)
        {
            $table->integer('usuario_responsable');
            $table->dropColumn('creado_por');
            $table->dropColumn('actualizado_por');
        });
    }
}
